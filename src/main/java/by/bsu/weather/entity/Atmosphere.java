package by.bsu.weather.entity;

class Atmosphere {
	private double humidity;
	private double visibility;
	private double pressure;
	private double rising;
	public Atmosphere() {
		super();
	}
	public Atmosphere(double humidity, double visibility, double pressure,
			double rising) {
		super();
		this.humidity = humidity;
		this.visibility = visibility;
		this.pressure = pressure;
		this.rising = rising;
	}
	public double getHumidity() {
		return humidity;
	}
	public void setHumidity(double humidity) {
		this.humidity = humidity;
	}
	public double getVisibility() {
		return visibility;
	}
	public void setVisibility(double visibility) {
		this.visibility = visibility;
	}
	public double getPressure() {
		return pressure;
	}
	public void setPressure(double pressure) {
		this.pressure = pressure;
	}
	public double getRising() {
		return rising;
	}
	public void setRising(double rising) {
		this.rising = rising;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(humidity);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(pressure);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(rising);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(visibility);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atmosphere other = (Atmosphere) obj;
		if (Double.doubleToLongBits(humidity) != Double
				.doubleToLongBits(other.humidity))
			return false;
		if (Double.doubleToLongBits(pressure) != Double
				.doubleToLongBits(other.pressure))
			return false;
		if (Double.doubleToLongBits(rising) != Double
				.doubleToLongBits(other.rising))
			return false;
		if (Double.doubleToLongBits(visibility) != Double
				.doubleToLongBits(other.visibility))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Atmosphere [humidity=" + humidity + ", visibility="
				+ visibility + ", pressure=" + pressure + ", rising=" + rising
				+ "]";
	}
	
}
