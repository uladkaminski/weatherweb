package by.bsu.weather.entity;

class Condition {
	private String text;
	private double temperature;
	public Condition() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Condition(String text, double temperature) {
		super();
		this.text = text;
		this.temperature = temperature;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public double getTemperature() {
		return temperature;
	}
	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(temperature);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Condition other = (Condition) obj;
		if (Double.doubleToLongBits(temperature) != Double
				.doubleToLongBits(other.temperature))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Conditon [text=" + text + ", temperature=" + temperature + "]";
	}
	
}
