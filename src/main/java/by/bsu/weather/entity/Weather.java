package by.bsu.weather.entity;


import by.bsu.weather.codes.ZipCodeHolder;
import by.bsu.weather.exception.WeatherException;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamReader;
import java.net.URL;
import java.util.Date;

public class Weather {
	private String city;
	private Date date;
	private Wind wind;
	private Atmosphere atmosphere;
	private Astronomy astronomy;
	private Condition condition;

	
	public Weather(String location) throws WeatherException {
		this.date = new Date();
		astronomy = new Astronomy();
		wind = new Wind();
		atmosphere = new Atmosphere();
		condition = new Condition();
		String text = null;
        ZipCodeHolder holder = new ZipCodeHolder();
            if(holder.get(location)!=null) {
                this.city = location;

                XMLInputFactory factory = XMLInputFactory.newInstance();
                try {
                    XMLStreamReader reader = factory.createXMLStreamReader(new URL("http://xml.weather.yahoo.com/forecastrss?p=" + holder.get(city) + "&u=c").openStream());
                    while (reader.hasNext()) {
                        int event = reader.next();
                        switch (event) {
                            case XMLStreamConstants.START_ELEMENT: {
                                switch (reader.getLocalName()) {
                                    case "wind": {
                                        wind.setChill(Double.parseDouble(reader.getAttributeValue(null, "chill")));
                                        wind.setDirection(Integer.parseInt(reader.getAttributeValue(null, "direction")));
                                        wind.setSpeed(Double.parseDouble(reader.getAttributeValue(null, "speed")));
                                        break;
                                    }
                                    case "astronomy": {
                                        astronomy.setSunrise(reader.getAttributeValue(null, "sunrise"));
                                        astronomy.setSunset(reader.getAttributeValue(null, "sunset"));
                                        break;
                                    }
                                    case "atmosphere": {
                                        atmosphere.setHumidity(Double.parseDouble(reader.getAttributeValue(null, "humidity")));
                                        atmosphere.setVisibility(Double.parseDouble(reader.getAttributeValue(null, "visibility")));
                                        atmosphere.setPressure(Double.parseDouble(reader.getAttributeValue(null, "pressure")));
                                        atmosphere.setRising(Double.parseDouble(reader.getAttributeValue(null, "rising")));
                                        break;
                                    }
                                    case "condition": {
                                        condition.setText(reader.getAttributeValue(null, "text"));
                                        condition.setTemperature(Double.parseDouble(reader.getAttributeValue(null, "temp")));
                                        break;
                                    }
                                }


                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }else {
                throw new WeatherException("Location is wrong!");
            }

    }

	public Weather(String city, Date date, Wind wind, Atmosphere atmosphere,
			Astronomy astronomy, Condition condition) {
		super();
		this.city = city;
		this.date = date;
		this.wind = wind;
		this.atmosphere = atmosphere;
		this.astronomy = astronomy;
		this.condition = condition;
	}




	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}

    public Date getSunrise(){
        return astronomy.getSunrise();
    }


    public Date getSunset(){
        return astronomy.getSunset();
    }

    public double getHumidity(){
        return atmosphere.getHumidity();
    }

    public double getPressure(){
        return atmosphere.getPressure();
    }

    public double getRising(){
        return atmosphere.getRising();
    }

    public double getVisibility(){
        return atmosphere.getVisibility();
    }

    public String getText() {
        return condition.getText();
    }

    public double getTemperature() {
        return condition.getTemperature();
    }

    public int getDirection() {
        return wind.getDirection();
    }

    public double getSpeed() {
        return wind.getSpeed();
    }

    public double getChill() {
        return wind.getChill();
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result
				+ ((astronomy == null) ? 0 : astronomy.hashCode());
		result = prime * result
				+ ((atmosphere == null) ? 0 : atmosphere.hashCode());
		result = prime * result
				+ ((condition == null) ? 0 : condition.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((wind == null) ? 0 : wind.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Weather other = (Weather) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (astronomy == null) {
			if (other.astronomy != null)
				return false;
		} else if (!astronomy.equals(other.astronomy))
			return false;
		if (atmosphere == null) {
			if (other.atmosphere != null)
				return false;
		} else if (!atmosphere.equals(other.atmosphere))
			return false;
		if (condition == null) {
			if (other.condition != null)
				return false;
		} else if (!condition.equals(other.condition))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (wind == null) {
			if (other.wind != null)
				return false;
		} else if (!wind.equals(other.wind))
			return false;
		return true;
	}

	
	@Override
	public String toString() {
		return "Weather [city=" + city + ", date=" + date + ", wind=" + wind
				+ ", atmosphere=" + atmosphere + ", astronomy=" + astronomy
				+ ", condition=" + condition + "]";
	}


}
