package by.bsu.weather.entity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class Astronomy {
	private Date sunrise;
	private Date sunset;
	public Astronomy() {
		super();
	}
	public Astronomy(Date sunrise, Date sunset) {
		super();
		this.sunrise = sunrise;
		this.sunset = sunset;
	}
	public Date getSunrise() {
		return sunrise;
	}
	public void setSunrise(Date sunrise) {
		this.sunrise = sunrise;
	}
	public Date getSunset() {
		return sunset;
	}
	public void setSunset(Date sunset) {
		this.sunset = sunset;
	}
	public void setSunrise(String sunrise) throws ParseException {
		this.sunrise = new SimpleDateFormat("hh:mm aa",Locale.ENGLISH).parse(sunrise);
	}
	public void setSunset(String sunset) throws ParseException {
		this.sunset = new SimpleDateFormat("hh:mm aa",Locale.ENGLISH).parse(sunset);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sunrise == null) ? 0 : sunrise.hashCode());
		result = prime * result + ((sunset == null) ? 0 : sunset.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Astronomy other = (Astronomy) obj;
		if (sunrise == null) {
			if (other.sunrise != null)
				return false;
		} else if (!sunrise.equals(other.sunrise))
			return false;
		if (sunset == null) {
			if (other.sunset != null)
				return false;
		} else if (!sunset.equals(other.sunset))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Astronomy [sunrise=" + sunrise + ", sunset=" + sunset + "]";
	}
	
}	
