package by.bsu.weather.entity;

class Wind {
	private double chill;
	private int direction;
	private double speed;
	public Wind() {
		super();
	}
	public Wind(double chill, int direction, double speed) {
		super();
		this.chill = chill;
		this.direction = direction;
		this.speed = speed;
	}
	public double getChill() {
		return chill;
	}
	public void setChill(double chill) {
		this.chill = chill;
	}
	public int getDirection() {
		return direction;
	}
	public void setDirection(int direction) {
		this.direction = direction;
	}
	public double getSpeed() {
		return speed;
	}
	public void setSpeed(double speed) {
		this.speed = speed;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(chill);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + direction;
		temp = Double.doubleToLongBits(speed);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Wind other = (Wind) obj;
		if (Double.doubleToLongBits(chill) != Double
				.doubleToLongBits(other.chill))
			return false;
		if (direction != other.direction)
			return false;
		if (Double.doubleToLongBits(speed) != Double
				.doubleToLongBits(other.speed))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Wind [chill=" + chill + ", direction=" + direction + ", speed="
				+ speed + "]";
	}
	
}
