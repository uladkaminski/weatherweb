package by.bsu.weather.codes;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by note on 19.05.2015.
 */
public class ZipCodeHolder extends HashMap<String, String> {
    private static final String FILE_PATH = "src/main/resources/codes.properties";
    public ZipCodeHolder() {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(FILE_PATH));
            String str;
            while ((str = reader.readLine()) != null) {
                String[] split = str.split("\\s");
                    if (split.length == 2) {
                        this.put(split[1], split[0]);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
