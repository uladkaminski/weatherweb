package by.bsu.weather.exception;

/**
 * Created by note on 19.05.2015.
 */
public class WeatherException extends Exception {
    public WeatherException() {
    }

    public WeatherException(Throwable cause) {
        super(cause);
    }

    public WeatherException(String message) {
        super(message);
    }

    public WeatherException(String message, Throwable cause) {
        super(message, cause);
    }

    public WeatherException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
